#! /usr/bin/env python3

def shows(pdict):
    [print(key + " => (" + type(value).__name__ + ": " + value.__str__() + ")") for key, value in pdict.items()]
