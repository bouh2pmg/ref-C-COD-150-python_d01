#!/usr/bin/env python3
## No Header
## 29/06/2017 - 17:04:26

def     double_return(dico):
    list1 = []
    list2 = []
    for key, value in dico.items():
        list1.append(key)
        list2.append(value)
    return list1, list2

#print(double_return({"a": 1, "b": 2}))
