#!/usr/bin/env python3
## No Header
## 29/06/2017 - 16:56:43

import sys

def my_args_handler(*s):
    res = ""
    for word in s:
        if (res != ""):
            res += ", "
        res += word
    print(res)

## my_args_handler("plop", "truc", "wesh")
