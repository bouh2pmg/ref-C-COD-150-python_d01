#!/usr/bin/env python3
## No Header
## 03/07/2017 - 16:07:48

def display_all(dic, i = 0):
    for i, value in enumerate(dic):
        print(str(i) + " -> (" + str(value.__class__) + ": " + str(value) + ")")

#display_all(["5", 4, 6])
