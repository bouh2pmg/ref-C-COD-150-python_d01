#! /usr/bin/env python3

def count_words(wlist):
	wdict = {}
	return {w: 1 if w not in wdict and not wdict.update({w: 1}) else wdict[w] + 1 for w in wlist}

