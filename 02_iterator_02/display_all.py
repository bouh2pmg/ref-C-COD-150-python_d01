#!/usr/bin/env python3
## No Header
## 03/07/2017 - 16:07:41

def display_all(dic):
    for key, value in dic.items():
        print("(" + str(value.__class__) + ": " + str(value) + ")")

#display_all({"a": "5", "b": 4, "c": 6})
