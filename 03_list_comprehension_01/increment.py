#! /usr/bin/env python3

def increment(plist):
    return [n if not isinstance(n, int) else n + 1 for n in plist]
