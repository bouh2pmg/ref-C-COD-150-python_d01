#! /usr/bin/env python3

import re

SUBDOMAIN = "[a-z]{1,63}"
DOMAIN = "(?!.{254,})((" + SUBDOMAIN + "\\.)*" + SUBDOMAIN + ")"
USER = '[a-z0-9\+\-\_\.%\*\\\\]{1,1023}'
EMAIL = r"" + USER + "@" + DOMAIN

print(EMAIL)
print(re.search(EMAIL, "hallelouia@gmail.com"))
