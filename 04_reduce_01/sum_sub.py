#! /usr/bin/env python3

import sys
from functools import reduce

if len(sys.argv) == 1:
    print(0)
else:
    print(reduce(lambda left, right:int(left) + int(right)
                 if (int(left) % 2 == 1) else
                 int(left) - int(right), sys.argv[1:]))

